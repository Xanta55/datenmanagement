-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 21. Apr 2019 um 13:34
-- Server-Version: 10.1.38-MariaDB
-- PHP-Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `teehersteller`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_kunde`
--

CREATE TABLE `tbl_kunde` (
  `id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Email` varchar(40) NOT NULL,
  `anschrift_id` int(11) NOT NULL COMMENT 'Fremdschlüssel',
  `Alter` int(4) NOT NULL,
  `BestellteTeesorten` int(11) NOT NULL COMMENT 'anzahl'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_kunde`
--

INSERT INTO `tbl_kunde` (`id`, `Name`, `Email`, `anschrift_id`, `Alter`, `BestellteTeesorten`) VALUES
(1, 'Klaus Hansen', 'Klaus.Hansen82@web.de', 1, 32, 3),
(2, 'Freddi Krüger', 'Freddies@gmx.de', 2, 95, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_kundenbewertung`
--

CREATE TABLE `tbl_kundenbewertung` (
  `TessortenId` int(11) NOT NULL,
  `KudnenId` int(11) NOT NULL,
  `Bewertung` int(2) NOT NULL,
  `Bewertungstext` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_kundenbewertung`
--

INSERT INTO `tbl_kundenbewertung` (`TessortenId`, `KudnenId`, `Bewertung`, `Bewertungstext`) VALUES
(1, 1, 5, 'eher mehh aber trinnkbar'),
(2, 2, 0, 'Top');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_kundenwunsch`
--

CREATE TABLE `tbl_kundenwunsch` (
  `id` int(11) NOT NULL,
  `Kundenid` int(11) NOT NULL,
  `text` text NOT NULL,
  `sortenempfehlung` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_kundenwunsch`
--

INSERT INTO `tbl_kundenwunsch` (`id`, `Kundenid`, `text`, `sortenempfehlung`) VALUES
(1, 1, 'Ich wünsche mir überhaupt eine Sorte die genisbar ist', 'Waldfrucht'),
(2, 2, 'Schokolade!', 'Schokolade');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_teesorte`
--

CREATE TABLE `tbl_teesorte` (
  `id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Verfuegbarkeit` tinyint(1) NOT NULL,
  `Beschreibung` text NOT NULL,
  `Preis` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_teesorte`
--

INSERT INTO `tbl_teesorte` (`id`, `Name`, `Verfuegbarkeit`, `Beschreibung`, `Preis`) VALUES
(1, 'LaurenzTeeKreation', 0, 'Dieser Tee wurde von einem teelibhaber kreirt', 6.5),
(2, 'Vanille', 1, 'Vanillen Tee', 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_verkauf`
--

CREATE TABLE `tbl_verkauf` (
  `ID` int(11) NOT NULL,
  `TeesrotenId` int(11) NOT NULL,
  `KundenId` int(11) NOT NULL,
  `MengeInKg` double NOT NULL,
  `Datum` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_verkauf`
--

INSERT INTO `tbl_verkauf` (`ID`, `TeesrotenId`, `KundenId`, `MengeInKg`, `Datum`) VALUES
(1, 1, 1, 200.5, '1980-05-22'),
(2, 2, 2, 0.5, '2019-01-01');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tlb_anschrift`
--

CREATE TABLE `tlb_anschrift` (
  `ID` int(11) NOT NULL,
  `strasse` varchar(50) NOT NULL,
  `hausnummer` int(11) NOT NULL,
  `postleitzahl` int(11) NOT NULL,
  `ort` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tlb_anschrift`
--

INSERT INTO `tlb_anschrift` (`ID`, `strasse`, `hausnummer`, `postleitzahl`, `ort`) VALUES
(1, 'MoenkhoferWeg', 1, 223562, 'Luebeck'),
(2, 'Birkenweg', 655, 23566, 'Kenia');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`id`, `name`, `password`) VALUES
(1, 'Hellbender', '123'),
(2, 'ineversurrenderi', 'ichbindoof');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `tbl_kunde`
--
ALTER TABLE `tbl_kunde`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `tbl_kundenwunsch`
--
ALTER TABLE `tbl_kundenwunsch`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `tbl_teesorte`
--
ALTER TABLE `tbl_teesorte`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `tbl_verkauf`
--
ALTER TABLE `tbl_verkauf`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `tlb_anschrift`
--
ALTER TABLE `tlb_anschrift`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `tlb_anschrift`
--
ALTER TABLE `tlb_anschrift`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
