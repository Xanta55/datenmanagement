package shop.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Table(name = "tbl_kunde")
@Entity @Getter @Setter
public class Kunde extends IdentifiedEntity {

    @Column(length = 30)
    String name;

    @Column(length = 100)
    String email;

    @Column(length = 100)
    int alter;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "AnschriftId", referencedColumnName = "ID")
    Anschrift anschrift;

    @Override
    public String toString() {
        return String.format("User[id=%d, name='%s']", id, name);
    }
}
