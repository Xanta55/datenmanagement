package shop.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "tlb_teesorteVerstecken")
@Entity
@Getter
@Setter
public class tea {

    @Column()
    String Name;
    @Column()
    boolean Verfuegbarkeit;
    @Column()
    String Beschreibung;
    @Column()
    double Preis;
}
