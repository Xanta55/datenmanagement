package shop.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import shop.model.Kunde;
import shop.model.User;

import java.util.Optional;

public interface KundeRepository extends CrudRepository<Kunde, Integer> {
    User findByName(@Param(value = "name") String name);

    @Override
    @RestResource(exported = false)
    Optional<Kunde> findById(Integer id);

    @RestResource(exported = false)
    void delete(Kunde kunde);
}
